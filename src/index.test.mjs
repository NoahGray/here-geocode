import test from 'tape'
import Here from './index'

const {HERE_ID, HERE_CODE} = process.env

const here = new Here({
  appId: HERE_ID,
  appCode: HERE_CODE
})

// Format: 42.3902,-71.1293;42.3312,-71.0228
const bbox = '49.0049,-123.3085;49.3946,-122.0812'

test('test geocode', async (t) => {
  const json = await here.search("4357 main", {
    mapview: bbox
  })

  t.ok(json.View, 'should have View key')
  t.ok(json.MetaInfo, 'should have MetaInfo key')
  t.ok(Array.isArray(json.View), 'should have View array')
  t.ok(Array.isArray(json.View[0].Result), 'should have View[0].Result array')
  t.end()
})

/* Apparently HERE does not have business/occupant names at all? */
// test('test geocode by name', async (t) => {
//   const json = await here.geocode(null, {
//     mapview: bbox,
//     name: "mcdonald's"
//   })

//   t.ok(json.View, 'should have View key')
//   t.ok(json.MetaInfo, 'should have MetaInfo key')
//   t.ok(Array.isArray(json.View), 'should have View array')
//   t.ok(Array.isArray(json.View[0].Result), 'should have View[0].Result array')
//   t.end()
// })
