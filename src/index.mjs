// @flow

import fetch from 'node-fetch'

type Props = {
  version?: string,
  appId: string,
  appCode: string
};

type QueryOptions = {
  mapview?: string
};

export default class Here {
  constructor (options: Props) {
    this.version = options.version
    this.appId = options.appId
    this.appCode = options.appCode
    this.methods = ['geocode', 'search']

    this.methods.map(method => {
      this[method] = async (query: string | null, opts?: QueryOptions) => {
        const url = new URL(`https://geocoder.api.here.com/${this.version || '6.2/'}${method}.json`)

        url.searchParams.append('app_code', this.appCode)
        url.searchParams.append('app_id', this.appId)

        if (query) {
          url.searchParams.append('searchtext', query)
        }

        if (opts) {
          for (const key in opts) {
            url.searchParams.append(key, opts[key])
          }
        }

        try {
          const response = await fetch(url)
          const json = await response.json()

          console.log(`

            ${url.toString()}

          `)

          return json.Response
        } catch (e) {
          console.error(e)
        }
      }
    })
  }
}
