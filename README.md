# HERE Node client (using promises)

Simple client for HERE geocoding searches. Supports

* `search.json`
* `geocode.json`

# Usage

See [the tests](src/index.test.mjs) for up-to-date usage examples.

```js
import Here from 'here-geocoder'

const here = new Here({
  appId: 'YOUR-APP-ID-STRING',
  appCode: 'YOUR-APP-CODE-STRING'
})

export async function myGeoFunction () {
  const result = await here.geocode('some search string', {
    mapview: '49.0049,-123.3085;49.3946,-122.0812',
    maxresults: 20
    // any other attributes here.
  })
}
```

See https://developer.here.com/documentation/geocoder/topics/resource-geocode.html for other possible attributes to pass into the second parameter.

# Development

The project uses `yarn`, `npm` should work too, but it will add a `package-lock.json` file which you may not want to commit.

```bash
yarn install
yarn run test
# OR
yarn run test:watch
```

# Contributors

* Noah Gray [gitlab](https://gitlab.com/NoahGray) [github](https://github.com/oknoah) - initial release and developer - noahgray@me.com